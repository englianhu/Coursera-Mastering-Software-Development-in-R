
<img src='文艺坊图库/coursera.jpg' height='100'> <img src='文艺坊图库/jhu.jpg' height='100'>

# Coursera Mastering Software Development in R

<!-- badges: start -->
[![Jekyll site CI](https://github.com/englianhu/Coursera-Mastering-Software-Development-in-R/actions/workflows/jekyll.yml/badge.svg)](https://github.com/englianhu/Coursera-Mastering-Software-Development-in-R/actions/workflows/jekyll.yml) macOS: [![R](https://github.com/englianhu/Coursera-Mastering-Software-Development-in-R/actions/workflows/R-macos.yml/badge.svg)](https://github.com/englianhu/Coursera-Mastering-Software-Development-in-R/actions/workflows/R-macos.yml) Ubuntu: [![R](https://github.com/englianhu/Coursera-Mastering-Software-Development-in-R/actions/workflows/R-ubuntu.yml/badge.svg)](https://github.com/englianhu/Coursera-Mastering-Software-Development-in-R/actions/workflows/R-ubuntu.yml)
<!-- badges: end -->

**大秦赋 (Chinese Emperor)**<br>
春秋战国《*礼记•经解*》<br>
孔子曰：『君子慎始，差若毫厘，缪以千里。』

> <span style='color:#FFEBCD; background-color:#D2B48C;'>**《礼记·经解》孔子曰：**</span><span style='color:#A9A9A9'; background-color:#696969;'>*「君子慎始。差若毫厘，谬以千里。」*</span>[^1]

*引用：[「快懂百科」《礼记•经解》](https://www.baike.com/wikiid/2225522569881832051?view_id=2tt3iw3blkq000)和[第一范文网：差之毫厘，谬以千里的故事](https://www.diyifanwen.com/chengyu/liuziyishangchengyugushi/2010051523105152347092749890.htm)和[「百度百科」春秋时期孔子作品《礼记•经解》](https://baike.baidu.com/item/%E7%A4%BC%E8%AE%B0%C2%B7%E7%BB%8F%E8%A7%A3/2523092)和[「當代中國」差之毫釐 謬以千里](https://www.ourchinastory.com/zh/2962/%E5%B7%AE%E4%B9%8B%E6%AF%AB%E9%87%90%20%E8%AC%AC%E4%BB%A5%E5%8D%83%E9%87%8C)*

[^1]: [HTML Color Codes](https://html-color.codes)

## Index

Specialization : [Mastering Software Development in R Specialization](https://www.coursera.org/specializations/r)

- Course 1 : [The R Programming Environment](https://www.coursera.org/learn/r-programming-environment)
- Course 2 : [Advanced R Programming](https://www.coursera.org/learn/advanced-r)
- Course 3 : [Building R Packages](https://www.coursera.org/learn/r-packages)
- Course 4 : [Building Data Visualization Tools](https://www.coursera.org/learn/r-data-visualization)
- Course 5 : [Mastering Software Development in R Capstone](https://www.coursera.org/learn/r-capstone)

There is a manual [Mastering Software Development in R.pdf](https://github.com/englianhu/Coursera-Mastering-Software-Development-in-R/blob/master/reference/Mastering%20Software%20Development%20in%20R.pdf) for these courses. An additional and completed bookdown web-based manual on [Mastering Software Development in R](https://bookdown.org/rdpeng/RProgDA/).

## Course 1 : The R Programming Environment

- Week 1 : Basic R Language
- Week 2 : Data Manipulation
- Week 3 : Text Processing, Regular Expression, & Physical Memory
- Week 4 : Large Datasets

`swirl::install_course("The R Programming Environment")`

- http://swirlstats.com/scn/rpe.html
- https://github.com/swirldev/The_R_Programming_Environment

Kindly refer to [The R Programming Environment](http://rpubs.com/englianhu/the-r-programming-environment) for more details about the course (includes quiz and answer, materials, resource, assignment etc).

Certificate : [Coursera 01 - The R Programming Environment.pdf](https://raw.githubusercontent.com/englianhu/Coursera-Mastering-Software-Development-in-R/cc7eb134e4335100d452b9e27cd1b504111f25b1/01%20The%20R%20Programming%20Environment/Coursera%2001%20-%20The%20R%20Programming%20Environment.pdf)

## Course 2 : Advanced R Programming

- Week 1 : Welcome to Advanced R Programming
- Week 2 : Functional Programming
- Week 3 : Debugging and Profiling
- Week 4 : Object-Oriented Programming

`swirl::install_course("Advanced R Programming")`

- http://swirlstats.com/scn/arp.html
- https://github.com/swirldev/Advanced_R_Programming

Kindly refer to [Advanced R Programming](http://rpubs.com/englianhu/advanced-r-programming) for more details about the course (includes quiz and answer, materials, resource, assignment etc).

Certificate : [Coursera 02 - Advanced R Programming.pdf](https://raw.githubusercontent.com/englianhu/Coursera-Mastering-Software-Development-in-R/cc7eb134e4335100d452b9e27cd1b504111f25b1/02%20Advanced%20R%20Programming/Coursera%2002%20-%20Advanced%20R%20Programming.pdf)

## Course 3 : Building R Packages

- Week 1 : Getting Started with R Packages
- Week 2 : Documentation and Testing
- Week 3 : Licensing, Version Control, and Software Design
- Week 4 : Continuous Integration and Cross Platform Development

Kindly refer to [GitHub `wk4package`](https://github.com/englianhu/wk4package)(previous [The `farsdata` R Package](https://github.com/englianhu/farsdata)) which is my R package repo for the assignment.

Certificate : [Coursera 03 - Building R Packages.pdf](https://raw.githubusercontent.com/englianhu/Coursera-Mastering-Software-Development-in-R/cc7eb134e4335100d452b9e27cd1b504111f25b1/03%20Building%20R%20Packages/Coursera%2003%20-%20Building%20R%20Packages.pdf)

## Course 4 : Building Data Visualization Tools

- Week 1 : Welcome to Building Data Visualization Tools
- Week 2 : Mapping and interactive plots
- Week 3 : The grid Package
- Week 4 : Building New Graphical Elements

Kindly refer to [Specialization in R : 04 Plotting with ggplot2](https://rpubs.com/englianhu/plotting-with-ggplot2) for more details about the course (includes quiz and answer, materials, resource, assignment etc).

Kindly refer to [GitHub `wk4package`](https://github.com/englianhu/wk4package)(previous [The `farsdata` R Package](https://github.com/englianhu/farsdata)) which is my R package repo for the assignment.

Certificate : [Coursera 04 - Building Data Visualization Tools.pdf](https://raw.githubusercontent.com/englianhu/Coursera-Mastering-Software-Development-in-R/cc7eb134e4335100d452b9e27cd1b504111f25b1/04%20Building%20Data%20Visualization%20Tools/Coursera%2004%20-%20Building%20Data%20Visualization%20Tools.pdf)

## Course 5 : Mastering Software Development in R Capstone

- Week 1 : Obtain and Clean the Data
- Week 2 : Building Geoms
- Week 3 : Building a Leaflet Map
- Week 4 : Documentation and Packaging
- Week 5 : Deployment
- Week 6 : Final Assessment

Kindly refer to [Specialization in R : 05 Capstone Assignment-1](https://rpubs.com/englianhu/MSDR-Capstone-Assignment-1) and [Specialization in R : 05 Capstone Assignment-2](https://rpubs.com/englianhu/848454) for more details about the course (includes quiz and answer, materials, resource, assignment etc).

Kindly refer to <https://github.com/englianhu/JHU_MSDR_Capstone> and <https://github.com/englianhu/MSDRCapstone> which is my R package repositories for the assignment for more details.

Certificate : [Coursera 05 - Mastering Software Development in R Capstone.pdf](https://raw.githubusercontent.com/englianhu/Coursera-Mastering-Software-Development-in-R/f700b1869f3a95bf2b4fbf577f6c9c708b2f9dfc/05%20Mastering%20Software%20Development%20in%20R%20Capstone/Coursera%2005%20-%20Mastering%20Software%20Development%20in%20R%20Capstone.pdf)

## Miscellaneous

There have a `RMarkdown` file and also a copy of `pdf` and `png` format certificate of accomplishment in every single course (folders inside repo).

![Awarded on `08 Nov 2021`](https://raw.githubusercontent.com/englianhu/Coursera-Mastering-Software-Development-in-R/master/文艺坊图库/Mastering%20Software%20Development%20in%20R%20-%20Specialization.png)

Above `png` file and feel free to download [Mastering Software Development in R - Specializatioin.pdf](https://raw.githubusercontent.com/englianhu/Coursera-Mastering-Software-Development-in-R/f700b1869f3a95bf2b4fbf577f6c9c708b2f9dfc/Mastering%20Software%20Development%20in%20R%20-%20Specialization.pdf).

## Coursera Verified Certificate

Certificate of Specialization : [Mastering Software Development in R Specialization](https://www.coursera.org/account/accomplishments/specialization/KTF8BDLMBLQC)

- Cert 1 : [The R Programming Environment](https://www.coursera.org/account/accomplishments/records/B4FEKWK27Q6R)
- Cert 2 : [Advanced R Programming](https://www.coursera.org/account/accomplishments/records/W8FRFA3F5AYC)
- Cert 3 : [Building R Packages](https://www.coursera.org/account/accomplishments/records/KKAPGXYJTL9Y)
- Cert 4 : [Building Data Visualization Tools](https://www.coursera.org/account/accomplishments/records/K6EC87PF4AKD)
- Cert 5 : [Mastering Software Development in R Capstone](https://www.coursera.org/account/accomplishments/records/W6XDFFQ62NDX)
- University : [Johns Hopkins University](https://www.jhu.edu)

## Reference

- [Dynamic Documents for R using R Markdown](https://rpubs.com/moviedo/322222) introduce some useful functions and also packages for R users.
- [Advanced R (1st Edition)](http://adv-r.had.co.nz) designed primarily for R users who want to improve their programming skills and understanding of the language. It should also be useful for programmers coming to R from other languages, as it explains some of R's quirks and shows how some parts that seem horrible do have a positive side.
- [Efficient R programming](https://csgillespie.github.io/efficientR) teach us how to use R programming efficiently.
- [Advanced R Solutions (1st Edition)](https://advanced-r-solutions-ed1.netlify.com) offers solutions to the exercises from Hadley Wickham’s book Advanced R (1st Edition).
- [Advanced R Solutions (2nd Edition)](https://advanced-r-solutions.rbind.io) provides solutions to the exercises from Hadley Wickham’s Advanced R, 2nd edition.
- [「统计之都」大家好，有没有兴趣翻译一下Hadley Wickham《Mastering Shiny》一书的源码？](https://d.cosx.org/d/423494-da-jia-hao-you-mei-you-xing-qu-fan-yi-yi-xia-hadley-wickhammastering-shiny-yi-shu-de-yuan-ma/11)
